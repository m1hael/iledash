import {Component, OnInit} from "@angular/core";
import {HttpLoaderService} from "@services/http-loader.service";

@Component({
    selector: "app-http-progress-bar",
    templateUrl: "./http-progress-bar.component.html",
    styleUrls: ["./http-progress-bar.component.scss"]
})
export class HttpProgressBarComponent implements OnInit {
    visible: boolean = false;

    constructor(private loaderService: HttpLoaderService) {
        this.loaderService.isLoading.subscribe((value) => {
            this.visible = value;
        });
    }

    ngOnInit(): void {}
}
