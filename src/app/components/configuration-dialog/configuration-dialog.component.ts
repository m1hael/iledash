import {Component, Inject, OnInit, ViewChild} from "@angular/core";
import {ConfigService} from "@services/config.service";
import {Configuration} from "@models/Configuration";
import {JsonEditorComponent, JsonEditorOptions} from "ang-jsoneditor";
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {ServiceDefinition} from "@models/ServiceDefinition";
import {ToastrService} from "ngx-toastr";
import {forkJoin, Observable} from "rxjs";
import {map} from "rxjs/operators";
import {HttpResponse} from "@angular/common/http";

@Component({
    selector: "app-configuration-dialog",
    templateUrl: "./configuration-dialog.component.html",
    styleUrls: ["./configuration-dialog.component.css"]
})
export class ConfigurationDialogComponent implements OnInit {
    @ViewChild(JsonEditorComponent, {static: false}) editor: JsonEditorComponent;

    title: string;

    configurationData = {};
    currentConfiguration: Configuration;
    serviceConfiguration: Configuration;
    instanceConfigurations: Array<Configuration> = [];
    serviceDefintion: ServiceDefinition;
    jsonEditorOptions: JsonEditorOptions;

    constructor(
        private configService: ConfigService,
        private dialogRef: MatDialogRef<ConfigurationDialogComponent>,
        private toastr: ToastrService,
        @Inject(MAT_DIALOG_DATA) data
    ) {
        this.jsonEditorOptions = new JsonEditorOptions();
        this.jsonEditorOptions.escapeUnicode = true;
        this.jsonEditorOptions.mode = "code";
        this.jsonEditorOptions.mainMenuBar = false;
        this.jsonEditorOptions.navigationBar = false;
        this.jsonEditorOptions.statusBar = false;

        this.serviceDefintion = data.context;
        this.configService.list(this.serviceDefintion.id).subscribe(
            (configs) => {
                for (const config of configs) {
                    if (config.instance) this.instanceConfigurations.push(config);
                    else {
                        this.serviceConfiguration = config;
                        this.selectConfiguration(this.serviceConfiguration);
                    }
                }

                if (!configs || configs.length === 0) {
                    this.serviceConfiguration = this.newConfiguration(this.serviceDefintion.id);
                    this.selectConfiguration(this.serviceConfiguration);
                }
            },
            (error) => {
                if (error.status && error.status === 404) {
                    this.serviceConfiguration = this.newConfiguration(this.serviceDefintion.id);
                    this.selectConfiguration(this.serviceConfiguration);
                } else this.toastr.error("Could not load configuration for this service.");
            }
        );
    }

    ngOnInit(): void {}

    cancel(): void {
        this.dialogRef.close();
    }

    private newConfiguration(serviceId: number): Configuration {
        return {
            id: 0,
            service: this.serviceDefintion.id,
            urlToken: "",
            job: "",
            instance: 0,
            data: "{}"
        };
    }

    ok(): void {
        if (this.currentConfiguration) {
            this.currentConfiguration.data = this.editor.getText();
            this.configService.persist(this.currentConfiguration).subscribe(
                (response) => {
                    // we let this open : this.dialogRef.close(response.body);
                    this.toastr.info("Saved configuration");
                },
                (error) => {
                    if (error.status && error.status === 403)
                        this.toastr.info("No authorization to create web service configuration.");
                    else this.toastr.error("An error occured on saving the web service configuration.");
                }
            );
        } else this.toastr.error("Could not save configuration.");
    }

    onDeleteConfiguration(): void {
        if (this.currentConfiguration?.id > 0) {
            this.configService.delete(this.currentConfiguration.id).subscribe(
                (response) => {
                    this.toastr.info("Deleted configuration");

                    if (this.currentConfiguration.instance) {
                        this.selectConfiguration(this.serviceConfiguration);
                        this.instanceConfigurations.splice(
                            this.instanceConfigurations.indexOf(this.currentConfiguration),
                            1
                        );
                    }
                },
                (error) => {
                    this.toastr.error("An error occured on deleting the web service configuration.");
                }
            );
        }
    }

    onNewInstance(): void {
        this.configService.newInstance(this.serviceDefintion.id).subscribe(
            (config) => {
                this.instanceConfigurations.push(config);
            },
            (error) => {
                this.toastr.error("An error occured on creating a new instance configuration.");
            }
        );
    }

    onShowPreviousConfiguration(): void {
        if (this.currentConfiguration === this.instanceConfigurations[0])
            this.selectConfiguration(this.serviceConfiguration);
        else
            this.selectConfiguration(
                this.instanceConfigurations[this.instanceConfigurations.indexOf(this.currentConfiguration) - 1]
            );
    }

    onShowNextConfiguration(): void {
        if (this.currentConfiguration === this.serviceConfiguration)
            this.selectConfiguration(this.instanceConfigurations[0]);
        else
            this.selectConfiguration(
                this.instanceConfigurations[this.instanceConfigurations.indexOf(this.currentConfiguration) + 1]
            );
    }

    onDeleteInstances(): void {
        if (!this.instanceConfigurations) return;

        const deletions: Observable<HttpResponse<Object>>[] = [];
        let deletionCount = 0;

        for (const c of this.instanceConfigurations) {
            if (!c.job) deletions.push(this.configService.delete(c.id));
        }

        if (deletions.length === 0) this.toastr.info("Nothing to delete.");
        else {
            forkJoin(deletions)
                .pipe(
                    map((responses) => {
                        for (const response of responses) {
                            if (response.status === 204) {
                                deletionCount += 1;
                                const parts = response.url.split("/");
                                this.removeInstanceConfiguration(Number.parseInt(parts[parts.length - 1]));
                            }
                        }
                    })
                )
                .subscribe((value) => {
                    this.toastr.info(`Deleted ${deletionCount} unused configurations.`);
                });
        }
    }

    private removeInstanceConfiguration(id: number): void {
        this.instanceConfigurations.splice(
            this.instanceConfigurations.findIndex((c) => (c.id = id)),
            1
        );

        if (this.currentConfiguration !== this.serviceConfiguration) {
            // check if the configuration is still in the available instance configurations
            if (!this.instanceConfigurations.includes(this.currentConfiguration))
                this.selectConfiguration(this.serviceConfiguration);
        }
    }

    private selectConfiguration(configuration: Configuration): void {
        this.currentConfiguration = configuration;
        this.title = this.currentConfiguration.instance ? "Instance Configuration" : "Service Definition Configuration";
        if (configuration.data) this.configurationData = JSON.parse(configuration.data);
        else this.configurationData = {};
    }
}
