import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {Configuration} from "@models/Configuration";
import {ConfirmDialogComponent} from "@components/confirm-dialog/confirm-dialog.component";
import {interval} from "rxjs";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {ServiceDefinitionService} from "@services/service-definition.service";
import {ToastrService} from "ngx-toastr";
import {EventService} from "@app/services/event.service";
import {AppEvent} from "@app/events/AppEvent";

@Component({
    selector: "app-running-instances-list",
    templateUrl: "./running-instances-list.component.html",
    styleUrls: ["./running-instances-list.component.css"]
})
export class RunningInstancesListComponent implements OnInit, OnDestroy {
    @Input("runningInstances") runningInstances = {};
    refreshSubscription: any;

    constructor(
        private toastr: ToastrService,
        private definitionService: ServiceDefinitionService,
        private dialog: MatDialog,
        private eventService: EventService
    ) {}

    ngOnInit(): void {}

    ngOnDestroy(): void {
        if (this.refreshSubscription) this.refreshSubscription.unsubscribe();
    }

    onStopInstance(configuration: Configuration): void {
        const options = new MatDialogConfig();
        options.disableClose = true;
        options.autoFocus = true;
        options.data = {
            context: configuration,
            title: "Stop Service Instance",
            description: `Do you really want to stop this service instance?`
        };

        const dialogRef = this.dialog.open(ConfirmDialogComponent, options);
        dialogRef.afterClosed().subscribe((data) => {
            if (!data) return; // dialog aborted

            this.definitionService.stop(configuration).subscribe(
                (response) => {
                    this.toastr.info("Stopping job " + configuration.job);
                    if (!this.refreshSubscription) this.startRefresh();
                },
                (error) => {
                    if (error.status === 404) {
                        // job no longer in system
                    } else this.toastr.error("Could not stop service instance.");
                }
            );
        });
    }

    private startRefresh(): void {
        this.refreshSubscription = interval(2000).subscribe((value) => {
            this.eventService.send(new AppEvent(AppEvent.Domain.Application, AppEvent.Action.Refresh));
        });
    }
}
