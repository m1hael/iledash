import {Component, Input, OnInit} from "@angular/core";
import {ServiceDefinition} from "@models/ServiceDefinition";
import {ToastrService} from "ngx-toastr";
import {StarterService} from "@services/starter.service";
import {ConfigService} from "@services/config.service";
import {EventService} from "@services/event.service";
import {AppEvent} from "@app/events/AppEvent";

@Component({
    selector: "app-not-running-instances-list",
    templateUrl: "./not-running-instances-list.component.html",
    styleUrls: ["./not-running-instances-list.component.css"]
})
export class NotRunningInstancesListComponent implements OnInit {
    @Input("serviceDefinitions") serviceDefinitions: Array<ServiceDefinition> = [];

    constructor(
        private configService: ConfigService,
        private starterService: StarterService,
        private toastr: ToastrService,
        private eventService: EventService
    ) {}

    ngOnInit(): void {}

    onStartInstance(serviceDefinition: ServiceDefinition): void {
        this.starterService.start(serviceDefinition).subscribe(
            (response) => {
                this.toastr.info("Starting service " + serviceDefinition.name);
                this.eventService.send(new AppEvent(AppEvent.Domain.Application, AppEvent.Action.Refresh));
            },
            (error) => {
                if (error.status === 412)
                    this.toastr.error(
                        "There is no free configuration available. Increase the max. # of instances to start another web service instance."
                    );
                else if (error.status === 409 && error.error.reason === "unresolved_placeholder")
                    this.toastr.error("Not all template placeholders could be resolved.");
                else this.toastr.error("Could not start the service " + serviceDefinition.name + ". " + error.error);
            }
        );
    }
}
