import {Component} from "@angular/core";
import {AppEvent} from "@app/events/AppEvent";
import {EventService} from "@services/event.service";

@Component({
    selector: "app-main-menu",
    templateUrl: "./main-menu.component.html",
    styleUrls: ["./main-menu.component.scss"]
})
export class MainMenuComponent {
    name: string;

    constructor(private eventService: EventService) {}

    onRefresh(): void {
        this.eventService.send(new AppEvent(AppEvent.Domain.Application, AppEvent.Action.Refresh));
    }
}
