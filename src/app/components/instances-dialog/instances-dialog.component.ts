import {Component, Inject, OnDestroy, OnInit} from "@angular/core";
import {ConfigService} from "@services/config.service";
import {Configuration} from "@models/Configuration";
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {ServiceDefinition} from "@models/ServiceDefinition";
import {StarterService} from "@services/starter.service";
import {ToastrService} from "ngx-toastr";
import {ServiceDefinitionService} from "@services/service-definition.service";
import {interval} from "rxjs";
import {ConfirmDialogComponent} from "../confirm-dialog/confirm-dialog.component";

@Component({
    selector: "app-instances-dialog",
    templateUrl: "./instances-dialog.component.html",
    styleUrls: ["./instances-dialog.component.css"]
})
export class InstancesDialogComponent implements OnInit, OnDestroy {
    instanceConfigurations: Array<Configuration> = [];
    serviceDefinition: ServiceDefinition;
    refreshSubscription: any;

    constructor(
        private configService: ConfigService,
        private starterService: StarterService,
        private definitionService: ServiceDefinitionService,
        private dialogRef: MatDialogRef<InstancesDialogComponent>,
        private toastr: ToastrService,
        private dialog: MatDialog,
        @Inject(MAT_DIALOG_DATA) data
    ) {
        this.serviceDefinition = data.serviceDefinition;

        this.loadData();
    }

    private loadData(): void {
        this.configService.list(this.serviceDefinition.id).subscribe(
            (configs) => {
                this.instanceConfigurations = [];
                for (const config of configs)
                    if (config.instance && config.job) this.instanceConfigurations.push(config);
            },
            (error) => {
                if (error.status && error.status !== 404)
                    this.toastr.error("Could not load configuration for this service.");
            }
        );
    }

    ngOnInit(): void {}

    ngOnDestroy(): void {
        if (this.refreshSubscription) this.refreshSubscription.unsubscribe();
    }

    close(): void {
        this.dialogRef.close();
    }

    onRefresh(): void {
        this.loadData();
    }

    onStopAllInstances(): void {
        const options = new MatDialogConfig();
        options.disableClose = true;
        options.autoFocus = true;
        options.data = {
            context: this.serviceDefinition,
            title: "Stop Service Instances",
            description: `Do you really want to stop all service instances?`
        };

        const dialogRef = this.dialog.open(ConfirmDialogComponent, options);
        dialogRef.afterClosed().subscribe((data) => {
            if (!data) return; // dialog aborted

            this.definitionService.stopAll(this.serviceDefinition).subscribe(
                (response) => {
                    this.toastr.info("Stopping all service instances of " + this.serviceDefinition.name);
                    if (!this.refreshSubscription) this.startRefresh();
                },
                (error) => {
                    this.toastr.error("An error occurred on stopping all services instances.");
                }
            );
        });
    }

    onStartInstance(): void {
        this.starterService.start(this.serviceDefinition).subscribe(
            (response) => {
                this.toastr.info("Starting service " + this.serviceDefinition.name);
                this.loadData();
            },
            (error) => {
                if (error.status === 412)
                    this.toastr.error(
                        "There is no free configuration available. Increase the max. # of instances to start another web service instance."
                    );
                else if (error.status === 409 && error.error.reason === "unresolved_placeholder")
                    this.toastr.error("Not all template placeholders could be resolved.");
                else
                    this.toastr.error(
                        "Could not start the service " + this.serviceDefinition.name + ". " + error.error
                    );
            }
        );
    }

    onStopInstance(configuration: Configuration): void {
        const options = new MatDialogConfig();
        options.disableClose = true;
        options.autoFocus = true;
        options.data = {
            context: configuration,
            title: "Stop Service Instance",
            description: `Do you really want to stop this service instance?`
        };

        const dialogRef = this.dialog.open(ConfirmDialogComponent, options);
        dialogRef.afterClosed().subscribe((data) => {
            if (!data) return; // dialog aborted

            this.definitionService.stop(configuration).subscribe(
                (response) => {
                    this.toastr.info("Stopping job " + configuration.job);
                    if (!this.refreshSubscription) this.startRefresh();
                },
                (error) => {
                    if (error.status === 404) {
                        // job no longer in system
                    } else this.toastr.error("Could not stop service instance.");
                }
            );
        });
    }

    private startRefresh(): void {
        this.refreshSubscription = interval(2000).subscribe((value) => {
            this.loadData();
        });
    }
}
