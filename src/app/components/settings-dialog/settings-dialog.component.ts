import {Component, Inject, OnInit} from "@angular/core";
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
    selector: "app-settings-dialog",
    templateUrl: "./settings-dialog.component.html",
    styleUrls: ["./settings-dialog.component.css"]
})
export class SettingsDialogComponent implements OnInit {
    scope: string;
    scopeId: number;

    constructor(
        private dialogRef: MatDialogRef<SettingsDialogComponent>,
        @Inject(MAT_DIALOG_DATA) data
    ) {
        this.scope = data.scope;
        this.scopeId = data.scopeId;
    }

    ngOnInit(): void {}

    close(): void {
        this.dialogRef.close();
    }
}
