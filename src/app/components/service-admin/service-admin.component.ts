import {Component, OnInit} from "@angular/core";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {ServiceGroup} from "@models/ServiceGroup";
import {ServiceGroupService} from "@app/services/service-group.service";
import {ServiceDefinition} from "@models/ServiceDefinition";
import {ServiceDefinitionDialogComponent} from "@components/service-definition-dialog/service-definition-dialog.component";
import {ServiceDefinitionService} from "@services/service-definition.service";
import {ToastrService} from "ngx-toastr";
import {ConfirmDialogComponent} from "../confirm-dialog/confirm-dialog.component";
import {HttpResponse} from "@angular/common/http";
import {ConfigurationDialogComponent} from "../configuration-dialog/configuration-dialog.component";
import {StarterService} from "@services/starter.service";
import {ConfigService} from "@services/config.service";
import {EventService} from "@services/event.service";
import {AppEvent} from "@app/events/AppEvent";
import {SettingsDialogComponent} from "../settings-dialog/settings-dialog.component";
import {InstancesDialogComponent} from "../instances-dialog/instances-dialog.component";

@Component({
    selector: "app-service-admin",
    templateUrl: "./service-admin.component.html",
    styleUrls: ["./service-admin.component.css"]
})
export class ServiceAdminComponent implements OnInit {
    groups: Array<ServiceGroup> = [];
    selectedGroup: number;
    serviceDefinitions: Array<ServiceDefinition> = [];
    eventSubscription: any;

    constructor(
        private eventService: EventService,
        private configService: ConfigService,
        private starterService: StarterService,
        private groupService: ServiceGroupService,
        private serviceDefinitionService: ServiceDefinitionService,
        private toastr: ToastrService,
        private dialog: MatDialog
    ) {}

    ngOnInit(): void {
        this.eventSubscription = this.eventService.subscribe((event: AppEvent) => {
            if (event.domain === AppEvent.Domain.Application && event.action === AppEvent.Action.Refresh)
                this.refresh();
        });

        this.loadGroups();
    }

    ngOnDestroy(): void {
        this.eventSubscription.unsubscribe();
    }

    private refresh(): void {
        this.loadGroups();
    }

    private loadGroups(): void {
        this.groupService.list().subscribe(
            (serviceGroups) => {
                this.groups = serviceGroups;
                this.groups.sort((g1, g2) => g1.name.localeCompare(g2.name));

                const filteredGroups = this.groups.filter((group) => group.id === this.selectedGroup);
                if (!filteredGroups || filteredGroups.length === 0) this.selectedGroup = undefined;

                if (this.selectedGroup) this.loadServiceDefinitions(this.selectedGroup);
            },
            (error) => this.toastr.error("Could not fetch service groups.")
        );
    }

    private loadServiceDefinitions(groupId: number): void {
        this.serviceDefinitionService.list(groupId).subscribe(
            (definitons) => {
                this.serviceDefinitions = [...definitons];
                this.serviceDefinitions.sort((s1, s2) => s1.name.localeCompare(s2.name));
            },
            (error) => {
                this.toastr.error("Could not fetch service definitions.");
                this.serviceDefinitions = [];
            }
        );
    }

    onSelectGroup(event: any): void {
        this.selectedGroup = Number.parseInt(event.value);
        this.loadServiceDefinitions(this.selectedGroup);
    }

    onAddService(): void {
        const options = new MatDialogConfig();
        options.disableClose = true;
        options.autoFocus = true;
        options.data = {groups: [...this.groups]};

        const dialogRef = this.dialog.open(ServiceDefinitionDialogComponent, options);
        dialogRef.afterClosed().subscribe((serviceDefiniton) => {
            if (!serviceDefiniton) return; // dialog aborted

            this.serviceDefinitions.push(serviceDefiniton);
            this.serviceDefinitions.sort((s1, s2) => s1.name.localeCompare(s2.name));

            this.toastr.success(`Service ${serviceDefiniton.name} saved.`);
        });
    }

    onEditService(serviceDefinition: ServiceDefinition): void {
        const options = new MatDialogConfig();
        options.disableClose = true;
        options.autoFocus = true;
        options.data = {groups: [...this.groups], context: serviceDefinition};

        const dialogRef = this.dialog.open(ServiceDefinitionDialogComponent, options);
        dialogRef.afterClosed().subscribe(
            (definition) => {
                if (!definition) return; // dialog aborted

                this.serviceDefinitions.splice(this.serviceDefinitions.indexOf(serviceDefinition), 1);
                if (definition.group === this.selectedGroup) {
                    this.serviceDefinitions.push(definition);
                    this.serviceDefinitions.sort((s1, s2) => s1.name.localeCompare(s2.name));
                }

                this.toastr.success(`Service ${definition.name} saved.`);
            },
            (error) => this.toastr.error("An error occured on deleting the web service definition.")
        );
    }

    onDeleteService(serviceDefinition: ServiceDefinition): void {
        const options = new MatDialogConfig();
        options.disableClose = true;
        options.autoFocus = true;
        options.data = {
            context: serviceDefinition,
            title: "Delete Web Service Definition",
            description: `Do you really want to delete the service definition ${serviceDefinition.name}?`
        };

        const dialogRef = this.dialog.open(ConfirmDialogComponent, options);
        dialogRef.afterClosed().subscribe((data) => {
            if (!data) return; // dialog aborted

            this.serviceDefinitionService.delete(serviceDefinition).subscribe(
                (response: HttpResponse<Object>) => {
                    if (response.status === 200 || response.status === 204) {
                        this.serviceDefinitions.splice(this.serviceDefinitions.indexOf(serviceDefinition), 1);

                        this.toastr.success("Web service definition deleted.");
                    } else this.toastr.info("Could not delete web service definition.");
                },
                (error) => {
                    if (error.status === 403) this.toastr.info("No authorization to delete web service definition.");
                    else this.toastr.error("An error occured on deleting the web service definition.");
                }
            );
        });
    }

    onStartService(serviceDefinition: ServiceDefinition): void {
        this.starterService.start(serviceDefinition).subscribe(
            (response) => {
                this.toastr.info("Starting service " + serviceDefinition.name);
            },
            (error) => {
                if (error.status === 412)
                    this.toastr.error(
                        "There is no free configuration available. Increase the max. # of instances to start another web service instance."
                    );
                else if (error.status === 409 && error.error.reason === "unresolved_placeholder")
                    this.toastr.error("Not all template placeholders could be resolved.");
                else this.toastr.error("Could not start the service " + serviceDefinition.name + ". " + error.error);
            }
        );
    }

    onEditConfiguration(serviceDefinition: ServiceDefinition): void {
        const options = new MatDialogConfig();
        options.autoFocus = true;
        options.data = {context: serviceDefinition};

        this.dialog.open(ConfigurationDialogComponent, options);
    }

    onEditSettings(serviceDefinition: ServiceDefinition): void {
        const options = new MatDialogConfig();
        options.autoFocus = true;
        options.data = {scope: "service", scopeId: serviceDefinition.id};

        this.dialog.open(SettingsDialogComponent, options);
    }

    onShowRunningInstances(serviceDefinition: ServiceDefinition): void {
        const options = new MatDialogConfig();
        options.autoFocus = true;
        options.data = {serviceDefinition};

        this.dialog.open(InstancesDialogComponent, options);
    }
}
