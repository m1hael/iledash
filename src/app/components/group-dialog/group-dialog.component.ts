import {Component, Inject, OnInit} from "@angular/core";
import {HttpResponse} from "@angular/common/http";
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {ServiceGroup} from "@models/ServiceGroup";
import {ServiceGroupService} from "@services/service-group.service";
import {ToastrService} from "ngx-toastr";

@Component({
    selector: "app-group-dialog",
    templateUrl: "./group-dialog.component.html",
    styleUrls: ["./group-dialog.component.css"]
})
export class GroupDialogComponent implements OnInit {
    group: ServiceGroup = {id: 0, name: "", description: "", icon: ""};

    constructor(
        private groupService: ServiceGroupService,
        private dialogRef: MatDialogRef<GroupDialogComponent>,
        private toastr: ToastrService,
        @Inject(MAT_DIALOG_DATA) data
    ) {
        if (data && data.context) this.group = data.context;
    }

    ngOnInit(): void {}

    cancel(): void {
        this.dialogRef.close();
    }

    ok(): void {
        this.groupService.persist(this.group).subscribe(
            (response: HttpResponse<ServiceGroup>) => {
                if (response.status === 200 || response.status === 201)
                    this.dialogRef.close(this.group);
                else this.toastr.info("Could not save web service group.");
            },
            (error) => {
                if (error.status === 403)
                    this.toastr.info("No authorization to create web service group.");
                else if (error.status === 409) this.toastr.error("Duplicate group name.");
                else this.toastr.error("An error occured on saving the web service group.");
            }
        );
    }
}
