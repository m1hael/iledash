import {Component, OnInit, Inject} from "@angular/core";
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
    selector: "app-confirm-dialog",
    templateUrl: "./confirm-dialog.component.html",
    styleUrls: ["./confirm-dialog.component.scss"]
})
export class ConfirmDialogComponent implements OnInit {
    title: string = "Confirmation";
    description: string;
    context: any;

    constructor(
        private dialogRef: MatDialogRef<ConfirmDialogComponent>,
        @Inject(MAT_DIALOG_DATA) data
    ) {
        if (data.title) this.title = data.title;
        this.description = data.description;
        this.context = data.context;
    }

    ngOnInit(): void {}

    cancel(): void {
        this.dialogRef.close();
    }

    ok(): void {
        this.dialogRef.close(this.context);
    }
}
