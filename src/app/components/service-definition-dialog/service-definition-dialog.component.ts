import {Component, Inject, OnInit} from "@angular/core";
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {ToastrService} from "ngx-toastr";
import {ServiceDefinition} from "@models/ServiceDefinition";
import {ServiceDefinitionService} from "@services/service-definition.service";
import {ServiceGroup} from "@models/ServiceGroup";
import {HttpResponse} from "@angular/common/http";

@Component({
    selector: "app-service-definition-dialog",
    templateUrl: "./service-definition-dialog.component.html",
    styleUrls: ["./service-definition-dialog.component.css"]
})
export class ServiceDefinitionDialogComponent implements OnInit {
    private readonly WARNING_THRESHOLD_MAX_INSTANCES = 20;

    groups: Array<ServiceGroup> = [];
    serviceDefinition: ServiceDefinition = {
        id: 0,
        name: "",
        description: "",
        group: 0,
        icon: "",
        submitJobTemplate:
            "SBMJOB CMD(CALL PGM(my_program)) ALWMLTTHD(*YES) JOB(my_job) JOBQ(QUSRNOMAX)",
        autostart: 0,
        maxInstances: 1,
        configurable: false,
        version: 0
    };

    constructor(
        private serviceDefinitionService: ServiceDefinitionService,
        private dialogRef: MatDialogRef<ServiceDefinitionDialogComponent>,
        private toastr: ToastrService,
        @Inject(MAT_DIALOG_DATA) data
    ) {
        this.groups = data.groups;
        if (data.context) this.serviceDefinition = {...data.context};
    }

    ngOnInit(): void {}

    cancel(): void {
        this.dialogRef.close();
    }

    ok(): void {
        this.serviceDefinitionService.persist(this.serviceDefinition).subscribe(
            (response: HttpResponse<ServiceDefinition>) => {
                if (response.status === 200 || response.status === 201) {
                    if (
                        this.serviceDefinition.maxInstances > this.WARNING_THRESHOLD_MAX_INSTANCES
                    ) {
                        this.toastr.warning(
                            "You configured this service for more than " +
                                this.WARNING_THRESHOLD_MAX_INSTANCES +
                                " parallel running instances. Is this really what you wanted?",
                            "Max Instances",
                            {timeOut: 0}
                        );
                    }

                    this.dialogRef.close(response.body);
                } else this.toastr.info("Could not save web service definition.");
            },
            (error) => {
                if (error.status === 403)
                    this.toastr.info("No authorization to create web service definition.");
                else if (error.status === 409 && error.error.reason === "version_mismatch")
                    this.toastr.error(
                        "The web service definition has already been changed on the server. Please refresh and try again."
                    );
                else if (error.status === 409 && error.error.reason === "duplicate_key")
                    this.toastr.error(
                        "There is already a service with this name in this group. Please rename the service definition."
                    );
                else this.toastr.error("An error occured on saving the web service definition.");
            }
        );
    }
}
