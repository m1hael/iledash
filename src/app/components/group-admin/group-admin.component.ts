import {Component, OnDestroy, OnInit} from "@angular/core";
import {ConfirmDialogComponent} from "@components/confirm-dialog/confirm-dialog.component";
import {GroupDialogComponent} from "@components/group-dialog/group-dialog.component";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {ServiceGroup} from "@app/models/ServiceGroup";
import {ServiceGroupService} from "@services/service-group.service";
import {ToastrService} from "ngx-toastr";
import {HttpResponse} from "@angular/common/http";
import {EventService} from "@services/event.service";
import {AppEvent} from "@app/events/AppEvent";
import {SettingsDialogComponent} from "@components/settings-dialog/settings-dialog.component";

@Component({
    selector: "app-group-admin",
    templateUrl: "./group-admin.component.html",
    styleUrls: ["./group-admin.component.css"]
})
export class GroupAdminComponent implements OnInit, OnDestroy {
    groups: Array<ServiceGroup> = [];
    eventSubscription: any;

    constructor(
        private eventService: EventService,
        private groupService: ServiceGroupService,
        private toastr: ToastrService,
        private dialog: MatDialog
    ) {}

    ngOnInit(): void {
        this.eventSubscription = this.eventService.subscribe((event: AppEvent) => {
            if (
                event.domain === AppEvent.Domain.Application &&
                event.action === AppEvent.Action.Refresh
            )
                this.refresh();
        });

        this.loadGroups();
    }

    ngOnDestroy(): void {
        this.eventSubscription.unsubscribe();
    }

    private loadGroups(): void {
        this.groupService.list().subscribe(
            (serviceGroups) => {
                this.groups = serviceGroups;
                this.groups.sort((g1, g2) => g1.name.localeCompare(g2.name));
            },
            (error) => this.toastr.error("Could not fetch service groups.")
        );
    }

    private refresh(): void {
        this.loadGroups();
    }

    onAddGroup(): void {
        const options = new MatDialogConfig();
        options.disableClose = true;
        options.autoFocus = true;

        const dialogRef = this.dialog.open(GroupDialogComponent, options);
        dialogRef.afterClosed().subscribe((group) => {
            if (!group) return; // dialog aborted

            this.groups.push(group);
            this.groups.sort((g1, g2) => g1.name.localeCompare(g2.name));

            this.toastr.success(`Group ${group.name} saved.`);
        });
    }

    onEditGroup(group: ServiceGroup): void {
        const options = new MatDialogConfig();
        options.disableClose = true;
        options.autoFocus = true;
        options.data = {context: group};

        const dialogRef = this.dialog.open(GroupDialogComponent, options);
        dialogRef.afterClosed().subscribe((group) => {
            if (!group) return; // dialog aborted

            this.groups.sort((g1, g2) => g1.name.localeCompare(g2.name));

            this.toastr.success(`Group ${group.name} saved.`);
        });
    }

    onDeleteGroup(group: ServiceGroup): void {
        const options = new MatDialogConfig();
        options.disableClose = true;
        options.autoFocus = true;
        options.data = {
            context: group,
            title: "Delete Web Service Group",
            description: `Do you really want to delete the group ${group.name}?`
        };

        const dialogRef = this.dialog.open(ConfirmDialogComponent, options);
        dialogRef.afterClosed().subscribe((data) => {
            if (!data) return; // dialog aborted

            this.groupService.delete(group).subscribe(
                (response: HttpResponse<Object>) => {
                    if (response.status === 200 || response.status === 204) {
                        this.groups.splice(this.groups.indexOf(group), 1);

                        this.toastr.success("Web service group deleted.");
                    } else this.toastr.info("Could not delete web service group.");
                },
                (error) => {
                    if (error.status && error.status === 403) {
                        this.toastr.info("No authorization to delete web service group.");
                    } else {
                        this.toastr.error("An error occured on deleting the web service group.");
                    }
                }
            );
        });
    }

    onEditSettings(group: ServiceGroup): void {
        const options = new MatDialogConfig();
        options.autoFocus = true;
        options.data = {scope: "group", scopeId: group.id};

        this.dialog.open(SettingsDialogComponent, options);
    }
}
