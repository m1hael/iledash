import {Component, OnDestroy, OnInit} from "@angular/core";
import {AppEvent} from "@app/events/AppEvent";
import {ChartType, ChartOptions} from "chart.js";
import {ConfigService} from "@services/config.service";
import {EventService} from "@app/services/event.service";
import {ServiceDefinitionService} from "@services/service-definition.service";
import {SettingService} from "@app/services/setting.service";
import {SingleDataSet, Label} from "ng2-charts";
import {ToastrService} from "ngx-toastr";
import {UpdaterService} from "@app/services/updater.service";
import {ServiceDefinition} from "@app/models/ServiceDefinition";
import {zip} from "rxjs";
import {Configuration} from "@app/models/Configuration";
import {ServiceDefinitionDialogComponent} from "../service-definition-dialog/service-definition-dialog.component";

@Component({
    selector: "app-overview",
    templateUrl: "./overview.component.html",
    styleUrls: ["./overview.component.css"]
})
export class OverviewComponent implements OnInit, OnDestroy {
    chartLabels: Label[] = ["Running services", "Not started services"];
    chartData: SingleDataSet = undefined;
    chartType: ChartType = "doughnut";
    chartColors = [{backgroundColor: ["rgba(143,188,143,0.8)", "rgba(210,0,0,0.8)"]}];
    chartOptions: ChartOptions = {
        responsive: true,
        legend: {position: "bottom"}
    };

    listTitle: string;
    listData: Array<Object> = [];

    updateJob: string;
    eventSubscription: any;

    serviceDefinitionMap = {};
    runningInstances: Array<any> = [];
    notRunningCount: number = 0;
    notRunningServices: Array<ServiceDefinition> = [];
    groupedRunningInstances = {};

    showRunningInstancesList: boolean = false;
    showNotRunningInstancesList: boolean = false;

    constructor(
        private settingService: SettingService,
        private serviceDefinitionService: ServiceDefinitionService,
        private configService: ConfigService,
        private updaterService: UpdaterService,
        private eventService: EventService,
        private toastr: ToastrService
    ) {
        this.loadData();

        this.eventSubscription = this.eventService.subscribe((event: AppEvent) => {
            if (event.domain === AppEvent.Domain.Application) {
                if (event.action === AppEvent.Action.Refresh) this.loadData();
            }
        });
    }
    ngOnDestroy(): void {
        if (this.eventSubscription) this.eventSubscription.unsubscribe();
    }

    ngOnInit(): void {}

    private loadData(): void {
        this.settingService.resolveGlobalSetting("ILEDASH_UPDATE_JOB").subscribe(
            (setting) => (this.updateJob = setting.value),
            (error) => (this.updateJob = undefined)
        );

        const definitionsObserver = this.serviceDefinitionService.list();
        const configsObserver = this.configService.list();

        zip(definitionsObserver, configsObserver).subscribe(
            (data) => {
                this.notRunningCount = 0;
                this.runningInstances = [];
                this.groupedRunningInstances = {};
                this.notRunningServices = [];

                const [definitions, configs] = data;

                for (const config of configs) {
                    if (config.job) {
                        const serviceId = config.service.toString();
                        let group = this.groupedRunningInstances[serviceId];
                        if (!group) {
                            group = [];
                            this.groupedRunningInstances[serviceId] = group;
                        }

                        group.push(config);

                        this.runningInstances.push({...config});
                    }
                }

                for (const d of definitions) {
                    const serviceId = d.id.toString();
                    this.serviceDefinitionMap[serviceId] = d;

                    if (this.groupedRunningInstances[serviceId]) {
                        const notRunningInstances = d.autostart - this.groupedRunningInstances[serviceId].length;
                        // there may be more instances running than the autostart value
                        if (notRunningInstances > 0) {
                            this.notRunningCount += notRunningInstances;

                            if (!this.notRunningServices.includes(this.serviceDefinitionMap[serviceId]))
                                this.notRunningServices.push(this.serviceDefinitionMap[serviceId]);
                        }
                    } else {
                        this.notRunningCount += d.autostart;
                        if (d.autostart > 0 && !this.notRunningServices.includes(this.serviceDefinitionMap[serviceId]))
                            this.notRunningServices.push(this.serviceDefinitionMap[serviceId]);
                    }
                }

                for (const config of this.runningInstances)
                    config.serviceDefinition = this.serviceDefinitionMap[config.service.toString()];

                this.runningInstances = this.runningInstances.sort((c1, c2) =>
                    c1.serviceDefinition.name.localeCompare(c2.serviceDefinition.name)
                );

                this.chartData = [this.runningInstances.length, this.notRunningCount];
            },
            (error) => {
                this.toastr.error("Could not fetch data.");
            }
        );
    }

    onStartUpdateJob(): void {
        this.updaterService.start().subscribe(
            (response) => {
                this.toastr.info("Started updater");
                setTimeout(() => this.loadData(), 1000);
            },
            (error) => this.toastr.error("Could not start updater job.")
        );
    }

    onStopUpdateJob(): void {
        this.updaterService.stop().subscribe(
            (response) => {
                this.toastr.info("Stopped updater");
                setTimeout(() => this.loadData(), 5000);
            },
            (error) => this.toastr.error("Could not stop updater job.")
        );
    }

    chartClicked(event: any): void {
        this.listTitle = event.active[0]._model.label;

        if (event.active[0]._index === 0) {
            // show running
            this.showRunningInstancesList = true;
            this.showNotRunningInstancesList = false;
        } else {
            this.showRunningInstancesList = false;
            this.showNotRunningInstancesList = true;
        }
    }
}
