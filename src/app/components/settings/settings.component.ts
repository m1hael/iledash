import {AgGridAngular} from "ag-grid-angular";
import {Component, Input, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {EventService} from "@app/services/event.service";
import {Observable} from "rxjs";
import {Setting} from "@models/Setting";
import {SettingService} from "@services/setting.service";
import {ToastrService} from "ngx-toastr";
import {ValueSetterParams} from "ag-grid-community";
import {AppEvent} from "@app/events/AppEvent";

@Component({
    selector: "app-settings",
    templateUrl: "./settings.component.html",
    styleUrls: ["./settings.component.css"]
})
export class SettingsComponent implements OnInit, OnDestroy {
    @ViewChild("agGrid") agGrid: AgGridAngular;
    @Input() scope: string;
    @Input() scopeId: number;

    private eventSubscription: any;
    settings: Array<Setting> = [];
    gridContext: any = {};
    columnDefs = [
        {
            field: "name",
            headerName: "Name",
            width: "250px",
            resizable: true,
            editable: (params) => !params.node.data.validating,
            valueSetter: this.settingValueSetter,
            cellRenderer: this.settingNameRenderer
        },
        {
            field: "value",
            headerName: "Value",
            flex: 1,
            resizable: false,
            editable: (params) => !params.node.data.validating,
            valueSetter: this.settingValueSetter,
            cellRenderer: this.settingValueRenderer
        }
    ];

    constructor(
        private settingService: SettingService,
        private toastr: ToastrService,
        private eventServie: EventService
    ) {
        this.toastr = toastr;
        this.gridContext.settingService = this.settingService;
        this.gridContext.toastr = toastr;
    }

    ngOnInit(): void {
        this.eventSubscription = this.eventServie.subscribe((event: AppEvent) => {
            if (event.action === AppEvent.Action.Refresh) this.refreshData();
        });

        this.loadSettings();
    }

    ngOnDestroy(): void {
        this.eventSubscription.unsubscribe();
    }

    private refreshData(): void {
        this.loadSettings();
    }

    private loadSettings(): void {
        let settings: Observable<Array<Setting>>;
        if (this.scope === "global") settings = this.settingService.listGlobals();
        else settings = this.settingService.list(this.scope, this.scopeId);
        settings.subscribe(
            (s) => (this.settings = s),
            (error) => this.toastr.error("Could not load settings.")
        );
    }

    onAddSetting(): void {
        const setting: Setting = {
            id: 0,
            name: "new_setting_name",
            scope: this.scope,
            scopeId: !this.scopeId ? 0 : this.scopeId,
            value: "",
            version: 0
        };

        this.agGrid.api.applyTransaction({add: [setting]});
        const newSettingRowIndex = this.agGrid.api.getLastDisplayedRow();
        this.agGrid.api.startEditingCell({rowIndex: newSettingRowIndex, colKey: "name"});
    }

    private settingValueSetter(params: ValueSetterParams): any {
        const newSetting = {...params.node.data};
        newSetting[params.colDef.field] = params.newValue;

        params.data.validating = true;

        if (!params.newValue) {
            if (params.node.data.id === 0)
                params.api.applyTransactionAsync({remove: [params.node.data]});
            else
                params.context.settingService.delete(params.node.data).subscribe(
                    (response) => {
                        params.context.toastr.info("Setting deleted.");
                        params.api.applyTransactionAsync({remove: [params.node.data]});
                    },
                    (error) => params.context.toastr.error("Could not delete setting.")
                );
        } else {
            params.context.settingService.persist(newSetting).subscribe(
                (setting) => {
                    params.context.toastr.info("Setting saved.");
                    const data = params.data;
                    data.id = setting.id;
                    data.name = setting.name;
                    data.value = setting.value;
                    data.version = setting.version;
                    data.validating = false;
                    params.api.applyTransactionAsync({update: [data]});
                },
                (error) => {
                    if (error.status === 409 && error.error.reason === "duplicate_key")
                        params.context.toastr.error("Setting already exist.");
                    else if (error.status === 409 && error.error.reason === "version_mismatch")
                        params.context.toastr.error("Settings need to be reloaded.");
                    else params.context.toastr.error("Could not update setting.");

                    // TODO not working : cannot find object
                    console.log(JSON.stringify(params.data));
                    params.data.validating = false;
                    params.api.applyTransactionAsync({update: [params.data]});
                    // params.api.getRowNode(params.node.id).setData(params.data);
                }
            );
        }

        return true;
    }

    private settingValueRenderer(params): any {
        if (!params.data.validating) return params.data.value;
        else return params.data.value + " &nbsp; " + "<div class='spin'></div>";
    }

    private settingNameRenderer(params): any {
        if (!params.data.validating) return params.data.name;
        else return params.data.name + " &nbsp; " + "<div class='spin'></div>";
    }
}
