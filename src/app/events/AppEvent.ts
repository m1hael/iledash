export class AppEvent {
    domain: AppEvent.Domain;
    action: AppEvent.Action;
    payload: any;

    constructor(domain: AppEvent.Domain, action: AppEvent.Action, payload?: any) {
        this.domain = domain;
        this.action = action;
        this.payload = payload;
    }
}

export namespace AppEvent {
    export enum Action {
        Refresh
    }

    export enum Domain {
        Application
    }
}
