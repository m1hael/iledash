import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable, of} from "rxjs";
import {Configuration} from "@models/Configuration";
import {ApplicationConfigService} from "./application-config.service";

@Injectable({
    providedIn: "root"
})
export class ConfigService {
    private url: string;

    private httpOptions = {
        headers: new HttpHeaders({
            Accept: "application/json"
        })
    };

    constructor(private http: HttpClient, private appConfig: ApplicationConfigService) {
        this.url = appConfig.base;
    }

    get(id: number): Observable<Configuration> {
        return this.http.get<Configuration>(this.url + "/configuration/" + id, this.httpOptions);
    }

    getForService(service: number): Observable<Configuration> {
        return this.http.get<Configuration>(this.url + "/configuration/service/" + service, this.httpOptions);
    }

    persist(configuration: Configuration): Observable<HttpResponse<Configuration>> {
        return this.http.post<Configuration>(this.url + "/configuration", configuration, {
            headers: this.httpOptions.headers,
            observe: "response"
        });
    }

    list(service?: number): Observable<Configuration[]> {
        const url = service ? this.url + "/configuration?service=" + service : this.url + "/configuration";
        return this.http.get<Configuration[]>(url, this.httpOptions);
    }

    delete(id: number): Observable<HttpResponse<Object>> {
        return this.http.delete<HttpResponse<Object>>(this.url + "/configuration/" + id, {
            observe: "response"
        });
    }

    newInstance(serviceId: number): Observable<Configuration> {
        return this.http.post<Configuration>(
            this.url + "/configuration/service/" + serviceId + "/instance",
            {},
            {headers: this.httpOptions.headers, observe: "body"}
        );
    }
}
