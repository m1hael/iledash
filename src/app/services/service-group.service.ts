import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable, of} from "rxjs";
import {ServiceGroup} from "@models/ServiceGroup";
import {ApplicationConfigService} from "./application-config.service";

@Injectable({
    providedIn: "root"
})
export class ServiceGroupService {
    private url: string;

    private httpOptions = {
        headers: new HttpHeaders({
            Accept: "application/json"
        })
    };

    constructor(private http: HttpClient, private appConfig: ApplicationConfigService) {
        this.url = appConfig.base;
    }

    list(): Observable<ServiceGroup[]> {
        return this.http.get<ServiceGroup[]>(this.url + "/group", this.httpOptions);
    }

    persist(group: ServiceGroup): Observable<HttpResponse<ServiceGroup>> {
        return this.http.post<ServiceGroup>(this.url + "/group", group, {
            headers: this.httpOptions.headers,
            observe: "response"
        });
    }

    delete(group: ServiceGroup): Observable<HttpResponse<Object>> {
        return this.http.delete<HttpResponse<Object>>(this.url + "/group/" + group.id, {
            observe: "response"
        });
    }
}
