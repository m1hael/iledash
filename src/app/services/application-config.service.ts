import {environment} from "@environments/environment";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class ApplicationConfigService {
    base: string;
    starter: string;
    updater: string;

    constructor(private http: HttpClient) {
        this.base = environment.url.base;
        this.starter = environment.url.starter;
        this.updater = environment.url.updater;
    }

    load(): Promise<any> {
        const promise = this.http
            .get("/assets/config.json")
            .toPromise()
            .then((data) => {
                console.log("Loaded config: ", JSON.stringify(data));
                Object.assign(this, data);
                return data;
            });

        return promise;
    }
}
