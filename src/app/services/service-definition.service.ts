import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {ServiceDefinition} from "@models/ServiceDefinition";
import {Configuration} from "@models/Configuration";
import {ApplicationConfigService} from "./application-config.service";

@Injectable({
    providedIn: "root"
})
export class ServiceDefinitionService {
    private url: string;

    private httpOptions = {
        headers: new HttpHeaders({
            Accept: "application/json"
        })
    };

    constructor(private http: HttpClient, private appConfig: ApplicationConfigService) {
        this.url = appConfig.base;
    }

    list(groupId?: number): Observable<ServiceDefinition[]> {
        if (groupId)
            return this.http.get<ServiceDefinition[]>(this.url + `/service?group=${groupId}`, this.httpOptions);
        else return this.http.get<ServiceDefinition[]>(this.url + "/service", this.httpOptions);
    }

    persist(serviceDefiniton: ServiceDefinition): Observable<HttpResponse<ServiceDefinition>> {
        return this.http.post<ServiceDefinition>(this.url + "/service", serviceDefiniton, {
            headers: this.httpOptions.headers,
            observe: "response"
        });
    }

    delete(serviceDefiniton: ServiceDefinition): Observable<HttpResponse<Object>> {
        return this.http.delete<HttpResponse<Object>>(this.url + "/service/" + serviceDefiniton.id, {
            observe: "response"
        });
    }

    stop(configuration: Configuration): Observable<HttpResponse<Object>> {
        return this.http.delete<HttpResponse<Object>>(
            this.url + "/service/" + configuration.service + "/instance/" + configuration.id,
            {observe: "response"}
        );
    }

    stopAll(serviceDefinition: ServiceDefinition): Observable<HttpResponse<Object>> {
        return this.http.delete<HttpResponse<Object>>(this.url + "/service/" + serviceDefinition.id + "/instance", {
            observe: "response"
        });
    }
}
