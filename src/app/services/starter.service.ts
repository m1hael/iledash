import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {ServiceDefinition} from "@app/models/ServiceDefinition";
import {ApplicationConfigService} from "./application-config.service";

@Injectable({
    providedIn: "root"
})
export class StarterService {
    private url: string;

    constructor(private http: HttpClient, private appConfig: ApplicationConfigService) {
        this.url = appConfig.starter;
    }

    start(service: ServiceDefinition): Observable<HttpResponse<Object>> {
        const services = {service: service.id};

        return this.http.post(this.url, services, {
            observe: "response"
        });
    }
}
