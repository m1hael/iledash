import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable, of} from "rxjs";
import {Setting} from "@models/Setting";
import {ApplicationConfigService} from "./application-config.service";

@Injectable({
    providedIn: "root"
})
export class SettingService {
    private url: string;

    private httpOptions = {
        headers: new HttpHeaders({
            Accept: "application/json"
        })
    };

    constructor(private http: HttpClient, private appConfig: ApplicationConfigService) {
        this.url = appConfig.base;
    }

    listGlobals(): Observable<Array<Setting>> {
        return this.http.get<Array<Setting>>(this.url + "/setting/global", this.httpOptions);
    }

    list(scope: string, scopeId: number): Observable<Array<Setting>> {
        return this.http.get<Array<Setting>>(this.url + `/setting/${scope}/${scopeId}`, this.httpOptions);
    }

    persist(setting: Setting): Observable<Setting> {
        return this.http.post<Setting>(this.url + "/setting", setting, this.httpOptions);
    }

    delete(setting: Setting): Observable<HttpResponse<Object>> {
        return this.http.delete<HttpResponse<Object>>(this.url + "/setting/" + setting.id, {
            observe: "response"
        });
    }

    resolveGlobalSetting(name: string): Observable<Setting> {
        return this.http.get<Setting>(this.url + "/setting/global/" + name, this.httpOptions);
    }
}
