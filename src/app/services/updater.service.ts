import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {ApplicationConfigService} from "./application-config.service";

@Injectable({
    providedIn: "root"
})
export class UpdaterService {
    private url: string;

    constructor(private http: HttpClient, private appConfig: ApplicationConfigService) {
        this.url = appConfig.updater;
    }

    start(): Observable<HttpResponse<Object>> {
        return this.http.post(this.url, {}, {observe: "response"});
    }

    stop(): Observable<HttpResponse<Object>> {
        return this.http.delete(this.url, {observe: "response"});
    }
}
