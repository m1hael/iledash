export interface Configuration {
    id: number;
    service: number;
    urlToken: string;
    job: string;
    instance: number;
    data: string;
}
