export interface ServiceDefinition {
    id: number;
    name: string;
    description: string;
    group: number;
    icon: string;
    submitJobTemplate: string;
    configurable: boolean;
    autostart: number;
    maxInstances: number;
    version: number;
}
