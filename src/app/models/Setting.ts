export interface Setting {
    id: number;
    name: string;
    scope: string;
    scopeId: number;
    value: string;
    validating?: boolean;
    version: number;
}
