export interface ServiceGroup {
    id: number;
    name: string;
    description: string;
    icon: string;
}
