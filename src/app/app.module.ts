import {AppComponent} from "./app.component";
import {AppRoutingModule} from "./app-routing.module";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ConfigurationDialogComponent} from "./components/configuration-dialog/configuration-dialog.component";
import {ConfirmDialogComponent} from "@components/confirm-dialog/confirm-dialog.component";
import {FlexLayoutModule} from "@angular/flex-layout";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FormsModule} from "@angular/forms";
import {GroupAdminComponent} from "@components/group-admin/group-admin.component";
import {GroupDialogComponent} from "@components/group-dialog/group-dialog.component";
import {HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";
import {HttpLoaderInterceptorService} from "./services/http-loader-interceptor.service";
import {HttpLoaderService} from "./services/http-loader.service";
import {HttpProgressBarComponent} from "./components/http-progress-bar/http-progress-bar.component";
import {MainMenuComponent} from "./components/main-menu/main-menu.component";
import {NgJsonEditorModule} from "ang-jsoneditor";
import {APP_INITIALIZER, NgModule} from "@angular/core";
import {OverviewComponent} from "@components/overview/overview.component";
import {ServiceAdminComponent} from "@components/service-admin/service-admin.component";
import {ServiceDefinitionDialogComponent} from "@components/service-definition-dialog/service-definition-dialog.component";
import {SharedModule} from "@modules/shared/shared.module";
import {SideMenuComponent} from "@components/side-menu/side-menu.component";
import {ToastrModule} from "ngx-toastr";
import {SettingsComponent} from "./components/settings/settings.component";
import {AgGridModule} from "ag-grid-angular";
import {GlobalSettingsComponent} from "./components/global-settings/global-settings.component";
import {SettingsDialogComponent} from "./components/settings-dialog/settings-dialog.component";
import {InstancesDialogComponent} from "./components/instances-dialog/instances-dialog.component";
import {ChartsModule} from "ng2-charts";
import {RunningInstancesListComponent} from "./components/running-instances-list/running-instances-list.component";
import {NotRunningInstancesListComponent} from "./components/not-running-instances-list/not-running-instances-list.component";
import {ApplicationConfigService} from "./services/application-config.service";

export function appInit(appConfig: ApplicationConfigService) {
    return () => appConfig.load();
}

@NgModule({
    declarations: [
        AppComponent,
        HttpProgressBarComponent,
        MainMenuComponent,
        SideMenuComponent,
        OverviewComponent,
        GroupDialogComponent,
        ConfirmDialogComponent,
        ServiceAdminComponent,
        GroupAdminComponent,
        ServiceDefinitionDialogComponent,
        ConfigurationDialogComponent,
        SettingsComponent,
        GlobalSettingsComponent,
        SettingsDialogComponent,
        InstancesDialogComponent,
        RunningInstancesListComponent,
        NotRunningInstancesListComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        ChartsModule,
        FlexLayoutModule,
        FontAwesomeModule,
        FormsModule,
        HttpClientModule,
        NgJsonEditorModule,
        SharedModule,
        ToastrModule.forRoot(),
        AgGridModule.withComponents([])
    ],
    providers: [
        HttpLoaderService,
        {provide: HTTP_INTERCEPTORS, useClass: HttpLoaderInterceptorService, multi: true},
        ApplicationConfigService,
        {provide: APP_INITIALIZER, useFactory: appInit, multi: true, deps: [ApplicationConfigService]}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
