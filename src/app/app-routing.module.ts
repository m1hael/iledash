import {NgModule} from "@angular/core";
import {GroupAdminComponent} from "@components/group-admin/group-admin.component";
import {GlobalSettingsComponent} from "@components/global-settings/global-settings.component";
import {OverviewComponent} from "@components/overview/overview.component";
import {Routes, RouterModule} from "@angular/router";
import {ServiceAdminComponent} from "@components/service-admin/service-admin.component";

const routes: Routes = [
    {path: "groups", component: GroupAdminComponent},
    {path: "services", component: ServiceAdminComponent},
    {path: "settings", component: GlobalSettingsComponent},
    {path: "**", component: OverviewComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
