export const environment = {
    production: true,
    url: {
        base: "http://localhost:45001/api",
        starter: "http://localhost:45003/api/starter",
        updater: "http://localhost:45003/api/updater"
    }
};
